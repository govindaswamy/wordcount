package com.kinetixtt.screening.service;

public interface TranslatorInterface {

	public String translate(final String word);
}
