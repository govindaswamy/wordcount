package com.kinetixtt.screening.service;

import com.kinetixtt.screening.service.exception.NonAlphabeticWordException;
import com.kinetixtt.screening.service.exception.ValidationFailedException;

public interface WordCounterInterface {
	
	/**
	 * Add a word to the local word counter
	 * Base language of this method is english
	 * This will convert all words to english and 
	 * store them in the counter
	 */
	public void addWord(final String word) throws ValidationFailedException, NonAlphabeticWordException;
	
	/**
	 * Returns the number of times a give word was added to the
	 * word counter. This method is language aware, so a count for 
	 * the same word in english and and spanish will return the same result
	 * 
	 * @param word
	 * @return
	 */
	public int countWord(final String word);
}
