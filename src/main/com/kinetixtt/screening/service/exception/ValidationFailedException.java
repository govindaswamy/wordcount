package com.kinetixtt.screening.service.exception;

public class ValidationFailedException extends Exception {

	private static final long serialVersionUID = -3383828999985543024L;

	public ValidationFailedException(final String message) {
		super(message);
	}
}
