package com.kinetixtt.screening.service.exception;

public class NonAlphabeticWordException extends Exception {

	private static final long serialVersionUID = -3383828999985543026L;

	public NonAlphabeticWordException(final String message) {
		super(message);
	}
}
