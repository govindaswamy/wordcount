package com.kinetixtt.screening.service;

public interface ValidatorInterface {

	/**
	 * Performs the intended validation
	 * on the input argument
	 * 
	 * @param word
	 * @return
	 */
	public boolean isValid(final String word);
}
