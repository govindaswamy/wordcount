package com.kinetixtt.screening.service.impl;

import java.util.HashMap;

import com.kinetixtt.screening.service.TranslatorInterface;
import com.kinetixtt.screening.service.ValidatorInterface;
import com.kinetixtt.screening.service.WordCounterInterface;
import com.kinetixtt.screening.service.exception.NonAlphabeticWordException;
import com.kinetixtt.screening.service.exception.ValidationFailedException;

public class WordCounterImpl implements WordCounterInterface {

	private HashMap<String, Integer> wordCounterCache;
	private TranslatorInterface translator;
	private ValidatorInterface validator;

	public WordCounterImpl(final TranslatorInterface translator) {
		wordCounterCache = new HashMap<>();
		this.translator = translator;
		validator = new AlphabeticWordValidatorImpl();
	}
	
	/**
	 * Adds an English Translated version of the provided word
	 * to the local word cache.
	 * Words are converted to lower case
	 */
	public void addWord(final String word) throws ValidationFailedException, NonAlphabeticWordException {
		if(word == null || word.length() < 1) {
			throw new ValidationFailedException("Cannot add an invalid word: " + word);
		}
		if(!validator.isValid(word))
		{
			throw new NonAlphabeticWordException("Cannot add alphanumeric word " + word);
		}
		String translatedWord = translator.translate(word).toLowerCase();
		Integer result = wordCounterCache.putIfAbsent(translatedWord, 1);
		if(result != null) {
			wordCounterCache.put(translatedWord, result + 1);
		}
	}

	/**
	 * Returns a count of the english translation of the input word
	 * Words are converted to lower case
	 */
	public int countWord(final String word) {
		if(word == null || word.length() < 1) {
			return 0;
		}
		String translatedWord = translator.translate(word).toLowerCase();
		if(!wordCounterCache.containsKey(translatedWord)) {
			return 0;
		}
		return wordCounterCache.get(translatedWord);
	}
}
