package com.kinetixtt.screening.service.impl;

import java.util.regex.Pattern;

import com.kinetixtt.screening.service.ValidatorInterface;

public class AlphabeticWordValidatorImpl implements ValidatorInterface {

	/**
	 * Validates the input argument to ensure that it only contains alphabets 
	 * and no special characters.
	 * returns false when argument is null.
	 */
	@Override
	public boolean isValid(final String word) {
		if(word == null) {
			return false;
		}
		Pattern pattern = Pattern.compile("[^a-zA-Z]");
		return !pattern.matcher(word).find();
	}
}
