package com.kinetixtt.screening.unittest.service;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.kinetixtt.screening.service.TranslatorInterface;
import com.kinetixtt.screening.service.WordCounterInterface;
import com.kinetixtt.screening.service.exception.NonAlphabeticWordException;
import com.kinetixtt.screening.service.exception.ValidationFailedException;
import com.kinetixtt.screening.service.impl.WordCounterImpl;

@RunWith(MockitoJUnitRunner.class)
public class WordCounterTest {

	@Mock
	private TranslatorInterface translator;
	private WordCounterInterface wordCounter;

	@Before
	public void setup() {
		wordCounter = new WordCounterImpl(translator);
	}
	
	@Test
	public void addASingleWordAndCheckCount() throws ValidationFailedException, NonAlphabeticWordException {
		String word = "Word";
		when(translator.translate(word)).thenReturn(word);
		wordCounter.addWord(word);
		assertTrue("Expected word count for the word: " + word + " is 1", wordCounter.countWord(word) == 1);
	}
	
	@Test(expected = ValidationFailedException.class)
	public void addNull() throws ValidationFailedException, NonAlphabeticWordException {
		wordCounter.addWord(null);
		fail("Exception was expected but not thrown");
	}
	
	@Test(expected = NonAlphabeticWordException.class)
	public void addNonAlphabeticWord() throws ValidationFailedException, NonAlphabeticWordException {
		String word = "ww21";
		when(translator.translate(word)).thenReturn(word);
		wordCounter.addWord(word);
	}
	
	@Test
	public void addSameWordTwiceAndCheckCount() throws ValidationFailedException, NonAlphabeticWordException {
		String word = "Word";
		when(translator.translate(any(String.class))).thenReturn(word);
		wordCounter.addWord(word);
		wordCounter.addWord(word);
		assertTrue("Expected word count for the word: " + word + " is 2 but got: " + wordCounter.countWord(word)
		, wordCounter.countWord(word) == 2);
	}

	@Test
	public void checkCountWithoutAddingWord() throws ValidationFailedException, NonAlphabeticWordException {
		String word = "NewWord";
		when(translator.translate(word)).thenReturn(word);
		assertTrue("Expected word count for the word: " + word + " is 0", wordCounter.countWord(word) == 0);
	}

	@Test
	public void checkCountAddADifferentWord() throws ValidationFailedException, NonAlphabeticWordException {
		String word = "NewWord";
		String someWord = "SomeWord";
		when(translator.translate(word)).thenReturn(word);
		when(translator.translate(someWord)).thenReturn(someWord);
		assertTrue("Expected word count for the word: " + word + " is 0 but got: " + wordCounter.countWord(someWord)
			, wordCounter.countWord(someWord) == 0);
	}

	@Test
	public void AddGermanWordButCheckCountForTranslatedWord() throws ValidationFailedException, NonAlphabeticWordException {
		String word = "blume";
		String translatedWord = "flower";
		when(translator.translate(any(String.class))).thenReturn(translatedWord);
		wordCounter.addWord(word);
		assertTrue("Expected word count for the word: " + translatedWord + " is 1 but got:" + wordCounter.countWord(translatedWord)
			, wordCounter.countWord(translatedWord) == 1);
	}

	@Test
	public void AddGermanWordAndCheckCountForGermanWord() throws ValidationFailedException, NonAlphabeticWordException {
		String word = "blume";
		String translatedWord = "flower";
		when(translator.translate(any(String.class))).thenReturn(translatedWord);
		wordCounter.addWord(word);
		assertTrue("Expected word count for the word: " + word + " is 1 but got:" + wordCounter.countWord(word)
			, wordCounter.countWord(word) == 1);
	}

	@Test
	public void AddGermanWordAndCheckCountForSpanishWord() throws ValidationFailedException, NonAlphabeticWordException {
		String germanWord = "blume";
		String spanishWord = "flor";
		String translatedWord = "flower";
		when(translator.translate(germanWord)).thenReturn(translatedWord);
		when(translator.translate(spanishWord)).thenReturn(translatedWord);
		wordCounter.addWord(germanWord);
		assertTrue("Expected word count for the word: " + spanishWord + " is 1 but got:" + wordCounter.countWord(spanishWord)
			, wordCounter.countWord(spanishWord) == 1);
	}

	@Test
	public void AddGermanAndSpanishWordAndCheckCount() throws ValidationFailedException, NonAlphabeticWordException {
		String germanWord = "blume";
		String spanishWord = "flor";
		String translatedWord = "flower";
		when(translator.translate(germanWord)).thenReturn(translatedWord);
		when(translator.translate(spanishWord)).thenReturn(translatedWord);
		when(translator.translate(translatedWord)).thenReturn(translatedWord);
		wordCounter.addWord(germanWord);
		wordCounter.addWord(spanishWord);
		assertTrue("Expected word count for the word: " + translatedWord + " is 2 but got:" + wordCounter.countWord(translatedWord)
			, wordCounter.countWord(translatedWord) == 2);
	}
	
	@Test
	public void checkCountForNull() throws ValidationFailedException, NonAlphabeticWordException {
		assertTrue("Expected word count for null is 0 but got:" + wordCounter.countWord(null)
			, wordCounter.countWord(null) == 0);
	}
	
	@Test
	public void checkCountForBlankWord() throws ValidationFailedException, NonAlphabeticWordException {
		String word = "";
		assertTrue("Expected word count for the word: " + word + " is 0 but got:" + wordCounter.countWord(word)
			, wordCounter.countWord(word) == 0);
	}
}

