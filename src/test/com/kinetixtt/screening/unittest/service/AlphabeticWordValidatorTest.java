package com.kinetixtt.screening.unittest.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import com.kinetixtt.screening.service.ValidatorInterface;
import com.kinetixtt.screening.service.impl.AlphabeticWordValidatorImpl;

public class AlphabeticWordValidatorTest {

	private static ValidatorInterface validator;
	
	@BeforeClass
	public static void setup() {
		validator = new AlphabeticWordValidatorImpl();
	}

	@Test
	public void validateAlphabeticWord() {
		String word = "Word";
		assertTrue("Expected True but was False", validator.isValid(word));
	}

	@Test
	public void validateAlphanumericWord() {
		String word = "Word123";
		assertFalse("Expected True but was False", validator.isValid(word));
	}

	@Test
	public void validateAlphanumericWordWithSpecialCharacters() {
		String word = "ahkc;';23";
		assertFalse("Expected True but was False", validator.isValid(word));
	}

	@Test
	public void validateAlphabeticWordWithSpecialCharactersNoNumbers() {
		String word = "kgkdf)*&^�";
		assertFalse("Expected True but was False", validator.isValid(word));
	}

	@Test
	public void validateAlphabeticWordWithUnicodeCharacter() {
		String word = "�kgkdfssfd";
		assertFalse("Expected True but was False", validator.isValid(word));
	}

	@Test
	public void validateEmptyString() {
		String word = "";
		assertTrue("Expected True but was False", validator.isValid(word));
	}

	@Test
	public void validateANull() {
		assertFalse("Expected True but was False", validator.isValid(null));
	}
}
