# WordCount
## Introduction

Is a simple application which can perform the following two operations:

- addWord method allows you to add words
- countWord method returns the count of how many times a given word was added to the word counter

## Features

The following validation features are supported

- Only Alphabetic words are allowed to be added.
- All methods are language aware and words are internally translated to english.

The interfaces used in the solution can be found in the ** package com.kinetixtt.screening.service **
There is no main method in the project. Unit Tests can be run to check the functionality of the system.

To run either use eclipse (import as a maven project) or maven.

## Further Enhancements

The code can be further enhanced by introducing Dependency Injection (Spring or Guice based). This would make the code more loosely coupled.